function keys(object) {
    let testObject = {}
    if (typeof object !== 'object') {
        return testObject
    }
    const keyArr = [];
    for (key in object) {
        keyArr.push(key);
    }
    return keyArr;
}

module.exports = keys;