function invert(obj) {
    let testObject = {}
    if (typeof obj !== 'object') {
        return testObject
    }
    const invertedObj = {};

    for (key in obj) {
        invertedObj[obj[key]]=key;
    }
    return invertedObj;
}

module.exports = invert;