function mapObject(object, cb) {
    let testObject = {}
    if (typeof object !== 'object') {
        return testObject
    }
    const newObject = {};
    for (key in object) {
        newObject[key] = cb(key, object);
    }
    return newObject;
}

module.exports = mapObject;