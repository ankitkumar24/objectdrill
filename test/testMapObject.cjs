let mapObject = require('../mapObject.cjs');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

let newObject = mapObject(testObject, (key, object) => {
    if (typeof object[key] === 'string') {
        return object[key].toUpperCase();
    }
    else {
        return testObject[key] += 10;
    }
});
console.log(newObject);