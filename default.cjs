function defaults(obj, defaultProps) {
    let testObject = {}
    if (typeof obj !== 'object' || defaultProps === undefined) {
        return testObject
    }
    let newObj = {};
    for (key in obj) {
        if (obj[key] == undefined) {
            newObj[key] = defaultProps;
        }
        else {
            newObj[key] = obj[key];
        }
    }
    return newObj;
}

module.exports = defaults;